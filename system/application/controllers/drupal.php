<?php

class Drupal extends Controller {
  function index() {
    $this->load->model('drupal_model');
    $data['result'] = $this->drupal_model->getTablesList();
    $data['page_title'] = "Drupal Database";
    $this->load->view('drupal_view', $data);
  }
}
