<?php
// controller class blog
class Blog extends Controller {

  function Blog() {
    parent::controller();
    $this->load->scaffolding('node');
    $this->load->library('embedded_video');
  }

  function index() {
    $output = '';
    $feed = '';

    $data['title'] = 'My blog title';
    $data['heading'] = 'My blog heading';
    $this->db->order_by('id', 'DESC');
    $query = $this->db->get('node');
    foreach ($query->result() as $row) {
      $output .= '<div class="node" style="border:1px solid #d2d2d2;padding: 10px;">';
      $output .= '<div class="title" style="padding: 10px;"><strong>'. $row->title .'</strong></div>';
      $output .= $this->embedded_video->get_video($row->video_url);
      //$output .= '</div>';
      $feed .= $this->embedded_video->get_feed($row->video_url);
     // $output .= $this->embedded_video->get_feed($row->video_url);
     // //$output .= '<div class="url">'. $row->video_url .'</div><br/><hr/>';
    }
    $data['content'] = $output;
    $data['content1'] = $feed;

    $this->load->view('blog_view', $data);
  }

}
