<?php

/*
 * @file
 * Hello world model file
 */

class Helloworld_model extends Model {

  /*
   * @function
   * contructor for helloworld
   */
  function Helloworld_model() {
    parent::Model();
  }

  /*
   * @function
   * Reads one or more row from table 'data'.
   */
  function getData() {
    $query = $this->db->get('data');
    if ($query->num_rows() > 0) {
      return $query->result();
    }
    else {
      show_error('No data found');
    }
  }

}
