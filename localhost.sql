-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 25, 2010 at 09:31 PM
-- Server version: 5.1.37
-- PHP Version: 5.2.10-2ubuntu6.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `test`
--

USE `test`;

-- --------------------------------------------------------

--
-- Table structure for table `node`
--

CREATE TABLE IF NOT EXISTS `node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `node`
--


-- --------------------------------------------------------

--
-- Table structure for table `nodes`
--

CREATE TABLE IF NOT EXISTS `nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `body` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `nodes`
--

INSERT INTO `nodes` (`id`, `title`, `body`) VALUES
(1, 'title 1', 'body corresponding to title 1'),
(2, 'title 2', 'body corresponding to title 2'),
(3, 'title 3', 'body corresponding to title 3'),
(4, 'title 4', 'body corresponding to title 4'),
(5, 'title 5', 'body corresponding to title 5'),
(6, 'title 6', 'body corresponding to title 6'),
(7, 'title 7', 'body corresponding to title 7'),
(8, 'title 8', 'body corresponding to title 8'),
(9, 'title 9', 'body corresponding to title 9');
